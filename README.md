===============================
hub2lab-hook
===============================


.. image:: https://img.shields.io/pypi/v/hub2lab-hook.svg
        :target: https://pypi.python.org/pypi/hub2lab-hook

.. image:: https://img.shields.io/travis/ant31/hub2lab-hook.svg
        :target: https://travis-ci.org/ant31/hub2lab-hook

.. image:: https://readthedocs.org/projects/hub2lab-hook/badge/?version=latest
        :target: https://hub2lab-hook.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/ant31/hub2lab-hook/shield.svg
     :target: https://pyup.io/repos/github/ant31/hub2lab-hook/
     :alt: Updates


Trigger gitlab build from github events
Features
--------

* TODO
